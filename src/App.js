import React from 'react';
import {  Container } from "semantic-ui-react";
import Contacts from './Contacts'
const App = ()  => {
  return (
    <Container>
      <Contacts/>
    </Container>
  );
}

export default App;
